Transforms the dongle into a virtual USB keyboard, that types your login credentials, when you press the button.

![alt text](Login_02.jpg)