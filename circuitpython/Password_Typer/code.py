# CircuitPython nRF52840 dongle - Password Typer


import time           # sleep function
import board          # board definitions (LEDs, switch, IOs)
import digitalio      # functions for digital IO 

from adafruit_hid.keyboard import Keyboard    # USB HID keyboard functions                
from adafruit_hid.keyboard_layout_us import KeyboardLayoutUS  # US keyboard layout
from adafruit_hid.keycode import Keycode  # keycodes for keypresses

time.sleep(1)  # Sleep for a bit to avoid a race condition on some systems
keyboard = Keyboard()  # instantiate the keyboard object!
keyboard_layout = KeyboardLayoutUS(keyboard)  # Set US layout

# Make the switch pin an input with pullups
key_pin = digitalio.DigitalInOut(board.SW1)
key_pin.direction = digitalio.Direction.INPUT
key_pin.pull = digitalio.Pull.UP

# Make the green LED pin an output
led = digitalio.DigitalInOut(board.LED1)
led.direction = digitalio.Direction.OUTPUT
led.value = True  # switch LED off (LED is connected to Vdd)

print("Waiting for button...")

while True:
    # main loop
    if not key_pin.value:  # wait for keypress
        print("Button pressed")
        # Turn on the green LED
        led.value = False

        while not key_pin.value:
            pass  # Wait till button is released again
        # "Type" the credentials
        keyboard_layout.write("username")  # ...Print the username
        keyboard.press(Keycode.TAB)  # "Press" the TAB key
        keyboard.release_all()  # ..."Release" the TAB key
        keyboard_layout.write("password") # Print the password
        keyboard.press(Keycode.ENTER)  # "Press" the ENTER key_pin
        keyboard.release_all()  # ..."Release" iter
        
        # Turn off the green LED
        led.value = True

    time.sleep(0.01) # some delay till the next loop